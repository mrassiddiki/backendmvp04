-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 06, 2020 at 10:31 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `digitalent_bank`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` bigint(20) NOT NULL,
  `id_account` longtext,
  `name` longtext,
  `password` longtext,
  `account_number` bigint(20) DEFAULT NULL,
  `saldo` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `id_account`, `name`, `password`, `account_number`, `saldo`) VALUES
(3, 'id-2799', 'Muhammad Rizki', '$2a$10$SARSXbLR8BllLjYxKxM6t.fsA7HslwyPmfeBRrqxvEW2PrwnvoC1.', 27146861, 0),
(4, 'id-414', 'Muhammad Assiddiki', '$2a$10$WoeY9vghiPJhw9VbPPcct.k45XUgzPkKTL3buW8oBmUQTj0Lt/zr.', 832712, 200000),
(5, 'id-650', 'Muhammad Rizki A', '$2a$10$/TgyeN/941HySBhE3tVosOhv.c.iYDa1L.MSahel1gM/RZq4rsUiy', 577718, 0),
(10, 'id-443', 'Muhammad Rizki Assiddiki', '$2a$10$yHFZwuxhSL9lmCSlHcqcXufuFnNZWaDRkQXFN.mLnAegDoy3EGRJ2', 621232, 500000);

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` bigint(20) NOT NULL,
  `transaction_type` bigint(20) DEFAULT NULL,
  `transaction_description` longtext,
  `sender` bigint(20) DEFAULT NULL,
  `amount` bigint(20) DEFAULT NULL,
  `recipient` bigint(20) DEFAULT NULL,
  `timestamp` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `transaction_type`, `transaction_description`, `sender`, `amount`, `recipient`, `timestamp`) VALUES
(1, 2, 'DEPOSIT KE REKENING', 621232, 1000000, 0, 1604457902),
(2, 0, 'TRANSFER DARI REKENING', 621232, 200000, 832712, 1604458056),
(3, 1, 'WITHDRAW DARI REKENING', 621232, 300000, 0, 1604458219);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
